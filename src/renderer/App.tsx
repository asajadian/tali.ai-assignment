import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import VoiceRecorder from '../components/VoiceRecorder';
import logo from '../../assets/tali-logo.svg';
import './App.css';

const Hello = () => {
  return (
    <>
      <header className="app-header">
        <img width="200px" alt="logo" src={logo} />
      </header>
      <section className="recorder-wrapper">
        <VoiceRecorder />
      </section>
      <h1>Tali.ai</h1>
    </>
  );
};

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Hello />} />
      </Routes>
    </Router>
  );
}
