import { useEffect, useRef, useState } from 'react';

const useRecorder = () => {
  const [recorder, setRecorder] = useState<MediaRecorder | null>(null);
  const [isRecording, setIsRecording] = useState(false);
  const streamChunks = useRef<Blob[]>([]);
  const onStopCb = useRef(() => {});

  const prepareRecorder = async () => {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    const _recorder = new MediaRecorder(stream);

    _recorder.ondataavailable = (e) => {
      streamChunks.current.push(e.data);
    };

    _recorder.onstart = () => {
      setIsRecording(true);
    };

    _recorder.onstop = () => {
      setIsRecording(false);
      onStopCb.current();
    };

    setRecorder(_recorder);
  };

  useEffect(() => {
    prepareRecorder();
  }, []);

  const makeBlob = (): Blob => {
    const blob = new Blob(streamChunks.current, { type: 'audio/wav' });
    return blob;
  };

  const start = () => {
    if (!recorder) return;
    if (recorder.state === 'recording') {
      throw new Error('Recorder already recording');
    }

    streamChunks.current = [];
    recorder.start();
  };

  const stop = (): Promise<Blob> => {
    if (!recorder) throw new Error('Recorder not initialized');
    if (recorder.state !== 'recording')
      throw new Error('Recorder not recording');

    return new Promise((resolve) => {
      onStopCb.current = () => {
        const recording = makeBlob();
        resolve(recording);
      };
      recorder.stop();
    });
  };

  return {
    _recorder: recorder,
    start,
    stop,
    isRecording,
  };
};

export default useRecorder;
