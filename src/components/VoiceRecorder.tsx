import React, { useState } from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrophone } from '@fortawesome/free-solid-svg-icons';
import useRecorder from '../hooks/useRecorder';
import './VoiceRecorder.scss';

const VoiceRecorder: React.FC = () => {
  const recorder = useRecorder();
  const [recordings, setRecordings] = useState<string[]>([]);

  const record = async () => {
    if (recorder.isRecording) {
      const recordingBlob = await recorder?.stop();
      const recording = URL.createObjectURL(recordingBlob);
      setRecordings((_recordings) => [..._recordings, recording]);
      return;
    }
    recorder.start();
  };

  return (
    <>
      <button
        type="button"
        className={classNames('record-button', {
          'record-button--active': recorder.isRecording,
        })}
        onClick={record}
      >
        <FontAwesomeIcon
          icon={faMicrophone}
          size="2x"
          color={recorder.isRecording ? 'white' : '#029B05'}
        />
      </button>
      <ul className="recordings-list">
        {recordings.map((recording) => (
          <li key={recording}>
            {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
            <audio preload="auto" controls>
              <source src={recording} />
            </audio>
          </li>
        ))}
      </ul>
    </>
  );
};

export default VoiceRecorder;
