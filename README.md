# Tali.ai Interview Assignment
This Project is the solution to the Tali.ai interview assignment. It is an electron app that records users microphone. It creates a list of recorded items and the user can listen to their recording.

## Project setup
```
npm install
```
### Development
To start the development server
```
npm start
```
## Packaging the app
Packaging the app and creating the installer package
```
npm run package
```
### Libraries and tools used
This solution is built on top of [react-typescript-boilerplate](https://github.com/react-boilerplate/react-boilerplate-typescript).
The only package used is the Fontawesome package.

### Components and hooks
The voice recording UI and logic is implemented in `VoiceRecorder` component. 

The `useRecorder` custom hook is also developed to abstract away navigator API usage and provide a simple and easy to use interface for accessing audio recording features.
